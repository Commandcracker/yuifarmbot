from pynput.keyboard import Controller, Key
import time
import json
import os
import atexit

keyboard = Controller()


def log(message):
    print('[' + time.strftime("%H:%M:%S") + "] " + message)


if __name__ == "__main__":
    # Settings #
    settings_file_path = os.path.join(os.path.dirname(__file__), "settings.json")
    if os.path.exists(settings_file_path):
        settings = json.load(open(settings_file_path, 'r'))
    else:
        settings = {
            "prefix": "y!",
            "commands": ["fish", "mine", "chop"],
            "time": {
                "start": {
                    "time": 3,
                    "enabled": True
                },
                "command": {
                    "time": 7,
                    "enabled": True
                },
                "loop": {
                    "time": 6.3,
                    "enabled": False
                }
            }
        }

    @atexit.register
    def dump_settings():
        json.dump(settings, open(settings_file_path, 'w'))

    log("Loaded")

    if settings["time"]["start"]["enabled"]:
        wait = settings["time"]["start"]["time"]
        log("Waiting " + str(wait) + "s to Select Discord")
        time.sleep(wait)

    loop = 0

    while True:
        for command in settings["commands"]:
            full_command = settings["prefix"] + command
            log("Typing: " + full_command)
            keyboard.press(Key.tab)
            keyboard.release(Key.tab)
            keyboard.type(full_command)
            keyboard.press(Key.enter)
            keyboard.release(Key.enter)
            if settings["time"]["command"]:
                wait = settings["time"]["command"]["time"]
                log("Waiting " + str(wait) + 's')
                time.sleep(wait)
        if settings["time"]["loop"]["enabled"]:
            wait = settings["time"]["loop"]["time"]
            log("Waiting " + str(wait) + 's')
            time.sleep(wait)
        loop = loop + 1
        log("Looped " + str(loop) + " Times")
